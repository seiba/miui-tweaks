# MIUI tweaks guide

## Website

- <https://seiba.gitlab.io/miui-tweaks/>
- Will plan on purchasing a domain if necessary

## Build requirements

- Python 3
- mkdocs
- pip (virtualenv highly recommended)

## How to run locally

- Ensure all dependencies are installed
- Run: ```mkdocs serve```

## Want to help improve this guide?

- Ensure you changes display properly locally
- If you are using VS Code as your editor, use the markdownlint extension
- Create a pull request

## License

- This project is licensed under the MIT license