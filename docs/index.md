# MIUI Tweaks Guide

## Background

If you are looking at this guide, the chances are that you are using a Xiaomi device and you are looking for ways to improve the experience of your device.
Rest assured, you have come to the right place.

Xiaomi is infamous for their ad and spyware-riddled operating system known as MIUI. Unfortunately Xiaomi isn't the only manufacturer known to do this, even Samsung have been caught doing the same as well.

While you could follow my steps if you don't own a Xiaomi phone, a lot of the tweaks I will be providing will be specific.

## Tweaks

### Debloating

Debloating is not only done to make your phone feel faster, ito can also reduce the amount of tracking information being sent to Xiaomi's servers.

Other benefits includes:

- Better battery life
- More available system memory

To do this, use Xiaomi ADB Fastboot Tools by Szaki:
<https://github.com/Szaki/XiaomiADBFastbootTools>

**Prerequisites**

- Your Xiaomi device
- A computer

**Steps**

1. Ensure you have the latest Java Runtime installed for your platform
2. Download and extract the latest release of ADB Fastboot Tools on your machine
3. On your Xiaomi device, enable developer options by tapping on the build number several times
4. Go to additional settings -> developer options and enable adb debugging
5. Connect your device to your machine
6. Open ADB Fastboot Tools
7. Allow your machine to use ADB debugging on your device
8. Under the Uninstall tab, select all the apps you want removed
9. Once all selected, apply the changes

**Recommended apps to remove**

App Name|Description|Side-effects
---|---|---
MSA|Miui system/service ads|NA
Joyose|Xiaomi tracking|NA
Browser|MIUI web browser. Privacy issues known|NA
Cleaner|App cleaner from Clean Master. Company has poor reputation on user privacy|NA
App Vault|App locker. Performance issues known|You may lose app locking

### Use an adblocker

Ads can be very annoying, but more importantly they are also a medium to introduce malware on your device. To remove ads, you can use an adblocker. Whilst it is true that on certain setups there is a system-wide toggle to disable ads, the remainder of users will not be able to have this option.

There are many different products you could use, each with their own advantages and disadvantages.

Method|Difficulty|Name|Installation
---|---|---|---
Local DNS Filter|Easy|Blokada|Run app in background
External DNS|Easy/Intermediate|Adguard DNS|Set as DNS resolver

### Reducing battery drain

Battery drain issues are more than often unique to the user. However there are some actions you could take that may help:

Action|Impact|Possible Consequence
---|---|---
Use automatic brightness adjustment or reduce your brightness|High|Content may not look as vibrant
Disable Bluetooth, GPS, Mobile Data when not in use|Medium/High|Have to re-enable for required apps
Disable AOD|Medium|Need to wake device for notifications
Use battery saver where possible|Low/Medium|Device may feel slower
Close all background apps|Low/Medium|Longer to open apps
Remove/limit antivirus apps (optional)|Low/Medium|Possible exposure to malware

### Use Google Camera (Gcam) for a better camera experience

One thing that has let down Xiaomi and their users is their lack of concern in the Camera quality department. 
To make things more sour, Xiaomi has been known to not support the latest advanced Camera APIs provided by Google, often leading to mediocre camera performance.

For some devices, there may be a way to use Gcam which can take advantage of Google's latest and greatest offerings such as HDR+, night sight and much more.[^1]

If you own a device that does support gcam, or if you simply want to try, you can download Gcam here: <https://www.celsoazevedo.com/files/android/google-camera/>

[^1]: More details on Camera API from Google: <https://source.android.com/devices/camera/versioning>

### Use a custom firmware

If you are still unhappy if the experience you are getting from MIUI, it may be possible to install a custom firmware for your device.

*Note: By installing a custom firmware, you will be voiding any warranty*

#### Unlocking your bootloader

If you've haven't done anything custom yet to your device, it is highly likely that your bootloader is locked. This bootloader comes locked from the factory to help prevent malicious code being run on your device. When unlocking, your phone will be completely wiped so please please please make a backup.

**Prerequisites**

- Your Xiaomi device that has been linked to your Xiaomi account
- At least 2-4 weeks have passed since linking your device[^2]
- A computer
- A backup of your data (seriously do it)

[^2]: Unfortunately there is no way around this delay. Whilst the exact period is not known, you must wait until the time has been passed

**Steps**

1. Download and extract the Xiaomi unlock tool onto your machine: <https://en.miui.com/unlock/>
2. Open the Xiaomi unlock tool and follow the prompts
3. (Optional) Restore your data

#### xiaomi.eu

One of the most well-known custom firmwares available to Xiaomi devices is Xiaomi.eu. It is based on the China MIUI firmware, which has many more features over Global firmware, but with all of the ads removed. 
It also supports a wide variety of languages that have been provided by the community. 

Install xiaomi.eu will not necessarily increase your user privacy, but it will give you the best Xiaomi experience.

For more information and to download, visit: <https://xiaomi.eu/community/>

#### LineageOS (and variants)

Say that you absolutely despise MIUI. Depending on what device you have, you may have a selection of custom firmwares you can flash. This is because community support will typically depend on the number of developers owning that device.

The most commonly seen custom firmware that isn't based on MIUI is LineageOS. There are many other custom firmwares too that are available, but again this really depends on your community.

To check what custom firmwares you can install on your device, search for your device over at XDA-developers: <https://forum.xda-developers.com/>

## Common problems

### No notifications on your apps

MIUI's battery management is known to be very aggressive to maximise battery life. Unfortunately it sometimes is unable to differentiate between important apps and less important ones.

**Steps**

1. In the settings app, open the battery menu
2. Open app battery saver (may be hidden in some menu)
3. Select the app that you want to fix notifications for
4. Tap save power and select no restrictions
5. Go back to the main settings menu and locate the affacted apps info
6. Turn on autostart

### Poor battery life after updating MIUI

You've downloaded that sweet MIUI update, but when you install it, you notice one thing that is bothering you. Your battery is draining a lot faster. 
 
Before you touch any settings, check your battery statistics. You will be able to see if you have any apps that are misbehaving or a resource hog


#### App is the culprit

Trying clearing the app cache. If this doesn't help, reinstall the app.

#### Android System is the culprit

This is a little more complicated. Traditionally whenever you perform a system update, Android will rebuild the cache for all your apps.
For some reason, Xiaomi have decided that your device will have to meet several conditions before this can be done.[^3]

[^3]: Taken from Mi Community Forums: <https://c.mi.com/thread-2834067-1-0.html>

When the app cache is not rebuilt, some may have been left at a poor state, leading to battery drainage. However you can trigger this task manually on your own.

**Prerequisites**

- A computer
- Your Xiaomi device
- Adb platform tools ([download here](https://dl.google.com/android/repository/platform-tools-latest-windows.zip))

**Steps**

1. Ensure ADB debugging has been enabled on your device
2. On your machine, open a command prompt in the location adb platform tools was extracted
3. Run: ```adb shell cmd package bg-dexopt-job```

Note: This will take a while to run depending on how many apps you have installed on your device. Once it has completed, your command prompt will simply display 'success'.

#### Something else

Battery drain can sometimes be a mysterious issue and it is rarely the case that the same issue is shared with many other users.

As I can not provide a solution for every unique problem, you should look deeply into your current setup.

### Beautify mode on selfie shots

Xiaomi and other Chinese phone manufacturers often enable beautify mode on selfie cameras by default. This is because a large majority of its users in Asia enjoy using this feature.

For others, you may find this as an annoyance. To make things worse, Xiaomi hides a lot of advanced settings. However you can unhide them.

**Steps**

1. Open the Camera app
2. Switch to selfie mode
3. Tap on the effects icon (star)
4. Tap on clear
5. Exit the camera app
6. Open your file explorer
7. Go to Internal Storage > DCIM > Camera
8. Create a folder named ```lab_options_visible```
9. Go back into the camera app
10. Open the setting menu
11. Disable beautify in portrait mode

## Other

### When will MIUI XYZ be available?

It will be available when Xiaomi makes it available. That's all to it. No tricks.

Can't wait? Install a custom firmware.

## Footnotes
